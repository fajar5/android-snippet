
## android snippets
### install adb & fastboot in linux
```
sudo apt install adb fastboot
```
### entering fastboot mode
```
adb reboot bootloader
```
### entering recovery mode
```
adb reboot recovery
```
### install twrp
```
// enter fastboot mode
fastboot flash recovery twrp-file.zip
```
### flash boot image
```
// enter fastboot mode
fastboot flash boot boot.img
```
### copy to phone
```
adb push file.txt /sdcard/Download/
```
### copy from phone
```
adb pull /sdcard/Download/file.txt
```
### install apk
```
adb install apps.apk
```

